from django.apps import AppConfig


class Sitelib1Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'siteLib1'
